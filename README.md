# Bug Pipeline Api Post Submodule

Reproduce bug with creating pipelines using the pipelines API and POST requests.

## token

- create a personal acceess token 
- give it 	api
- note it

then set in your terminal in variable TOKEN:
```
export TOKEN=xxdfgddsdsgdsfg
```

# ISSUE.

## what does work: curl with no parameters

```
curl --request POST -H "PRIVATE-TOKEN: $TOKEN" "https://gitlab.com/api/v4/projects/32998506/pipeline?ref=main"
```
ouput:
```
$ echo VAR=$VAR
VAR=
Cleaning up project directory and file based variables 00:01
Job succeeded
```

## also works, but not on our gitlab

```
curl  -H "Content-Type: application/json" -H "PRIVATE-TOKEN: $TOKEN"  -d '{"ref":"main", "variables":[{"key":"VAR", "value":"TOTO"}]}' https://gitlab.com/api/v4/projects/32998506/pipeline
```




